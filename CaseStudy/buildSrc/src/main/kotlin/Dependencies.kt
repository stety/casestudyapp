import org.gradle.api.artifacts.dsl.DependencyHandler

object Dependencies {
    object AndroidTools {
        const val buildGradle = "com.android.tools.build:gradle:${Versions.buildGradle}"
        const val desugarJdk = "com.android.tools:desugar_jdk_libs:${Versions.desugarJdk}"
    }

    object AndroidX {
        const val navigationSafeArgsPlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
        const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
        const val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
        const val navigationCompose = "androidx.navigation:navigation-compose:${Versions.navigation}"
        const val core = "androidx.core:core-ktx:${Versions.androidXCore}"
        const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
        const val cardView = "androidx.cardview:cardview:${Versions.cardView}"
        const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"
        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swiperefreshLayout}"
        const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
        const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}"
        const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
        const val lifecycleViewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.lifecycle}"
        const val activity = "androidx.activity:activity-ktx:${Versions.activity}"
        const val activityCompose = "androidx.activity:activity-compose:${Versions.activity}"
        const val composeMaterial = "androidx.compose.material:material:${Versions.compose}"
        const val composeAnimation = "androidx.compose.animation:animation:${Versions.compose}"
        const val composeUI = "androidx.compose.ui:ui-tooling:${Versions.compose}"
        const val pagingRuntime = "androidx.paging:paging-runtime-ktx:${Versions.paging}"
    }

    object Coroutines {
        const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
        const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    }

    object Espresso {
        const val core = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    }

    object Glide {
        const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    }

    object JUnit {
        const val junit = "junit:junit:${Versions.junit}"
        const val ext = "androidx.test.ext:junit:${Versions.junitExt}"
    }

    object Koin {
        const val android = "io.insert-koin:koin-android:${Versions.koin}"
        const val androidCompat = "io.insert-koin:koin-android-compat:${Versions.koin}"
        const val androidXWorkManager = "io.insert-koin:koin-androidx-workmanager:${Versions.koin}"
        const val androidXNavigation = "io.insert-koin:koin-androidx-navigation:${Versions.koin}"
        const val androidXCompose = "io.insert-koin:koin-androidx-compose:${Versions.koin}"
    }

    object Kotlin {
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinGradlePlugin}"
        const val serialization = "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlinSerialization}"
    }

    object KotlinX {
        const val serializationJson = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinXSerializationJson}"
        const val dateTime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinXDateTime}"
    }

    object MaterialDesign {
        const val material = "com.google.android.material:material:${Versions.materialDesign}"
    }

    object OkHttp {
        const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttpLoggingInterceptor}"
    }

    object Retrofit {
        const val retrofit2 = "com.squareup.retrofit2:retrofit:${Versions.retrofit2}"
        const val kotlinSerializationConverter =
            "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Versions.kotlinSerializationConverter}"
    }

    object Room {
        const val roomRuntime = "androidx.room:room-runtime:${Versions.room}"
        const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
        const val roomCommon = "androidx.room:room-common:${Versions.room}"
        const val room = "androidx.room:room-ktx:${Versions.room}"
    }

    object Timber {
        const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    }

    object ViewBindingPropertyDelegate {
        const val viewBindingPropertyDelegateNoReflection =
            "com.github.kirich1409:viewbindingpropertydelegate-noreflection:${Versions.viewBindingPropertyDelegate}"
    }

    val classpathLibraries = arrayListOf<String>().apply {
        add(AndroidTools.buildGradle)
        add(AndroidX.navigationSafeArgsPlugin)
        add(Kotlin.gradlePlugin)
        add(Kotlin.serialization)
    }

    val appLibraries = arrayListOf<String>().apply {
        add(AndroidX.navigationFragment)
        add(AndroidX.navigationUI)
        add(AndroidX.navigationCompose)
        add(AndroidX.core)
        add(AndroidX.appCompat)
        add(AndroidX.constraintLayout)
        add(AndroidX.cardView)
        add(AndroidX.fragment)
        add(AndroidX.swipeRefreshLayout)
        add(AndroidX.lifecycleViewModel)
        add(AndroidX.lifecycleRuntime)
        add(AndroidX.lifecycleLiveData)
        add(AndroidX.lifecycleViewModelCompose)
        add(AndroidX.activity)
        add(AndroidX.activityCompose)
        add(AndroidX.composeMaterial)
        add(AndroidX.composeAnimation)
        add(AndroidX.composeUI)
        add(AndroidX.pagingRuntime)
        add(Coroutines.coroutinesAndroid)
        add(Coroutines.coroutinesCore)
        add(Glide.glide)
        add(Koin.android)
        add(Koin.androidCompat)
        add(Koin.androidXCompose)
        add(Koin.androidXNavigation)
        add(Koin.androidXWorkManager)
        add(KotlinX.dateTime)
        add(KotlinX.serializationJson)
        add(MaterialDesign.material)
        add(OkHttp.loggingInterceptor)
        add(Retrofit.retrofit2)
        add(Retrofit.kotlinSerializationConverter)
        add(Room.room)
        add(Room.roomCommon)
        add(Room.roomRuntime)
        add(Timber.timber)
        add(ViewBindingPropertyDelegate.viewBindingPropertyDelegateNoReflection)
    }

    val annotationProcessorLibraries = arrayListOf<String>().apply {
        add(Room.roomCompiler)
    }

    val androidTestLibraries = arrayListOf<String>().apply {
        add(JUnit.ext)
        add(Espresso.core)
    }

    val testLibraries = arrayListOf<String>().apply {
        add(JUnit.junit)
    }

    val coreLibraryDesugaringLibraries = arrayListOf<String>().apply {
        add(AndroidTools.desugarJdk)
    }
}

//util functions for adding the different type dependencies from build.gradle.kts file
fun DependencyHandler.annotationProcessor(list: List<String>) {
    list.forEach { dependency ->
        add("annotationProcessor", dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add("implementation", dependency)
    }
}

fun DependencyHandler.androidTestImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("androidTestImplementation", dependency)
    }
}

fun DependencyHandler.testImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("testImplementation", dependency)
    }
}

fun DependencyHandler.classpathLibraries(list: List<String>) {
    list.forEach { dependency ->
        add("classpath", dependency)
    }
}

fun DependencyHandler.coreLibraryDesugaring(list: List<String>) {
    list.forEach { dependency ->
        add("coreLibraryDesugaring", dependency)
    }
}