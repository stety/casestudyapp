import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.*

plugins {
    kotlin("android")
    kotlin("kapt")
    id("com.android.application")
    id("kotlinx-serialization")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdk = AppConfig.compileSdk
    defaultConfig {
        applicationId = applicationId
        minSdk = AppConfig.minSdk
        targetSdk = AppConfig.targetSdk
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        testInstrumentationRunner = AppConfig.androidTestInstrumentation
    }
    buildTypes {
        getByName("debug") {
            isDebuggable = true
            versionNameSuffix = ".debug"
        }
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    flavorDimensions.add("api")
    productFlavors {
        create("Prod") {
            buildConfigField("String", "API_BASE_URL", "\"${AppConfig.apiUrl}\"")
            resValue("string", "app_name", "@string/app_name_test")
            applicationIdSuffix = ".test"
        }
        create("Test") {
            buildConfigField("String", "API_BASE_URL", "\"${AppConfig.apiUrl}\"")
            resValue("string", "app_name", "@string/app_name_prod")
        }
    }
    applicationVariants.all {
        resValue(
            "string",
            "APP_VERSION",
            versionName + "-" + versionCode + " " + getDate()
        )
        outputs.all {
            (this as com.android.build.gradle.internal.api.BaseVariantOutputImpl).outputFileName =
                "casestudy-${flavorName}-${versionName}.apk"
        }
    }
    compileOptions {
        // Flag to enable support for the new language APIs
        isCoreLibraryDesugaringEnabled = true

        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks {
        withType<KotlinCompile> {
            kotlinOptions.jvmTarget = "1.8"
        }
    }
    buildFeatures {
        viewBinding = true
        dataBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.composeKotlinCompilerExtensionVersion
    }
}

fun getDate(): String {
    val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.US)
    return simpleDateFormat.format(Date())
}

dependencies {
    implementation(Dependencies.appLibraries)
    annotationProcessor(Dependencies.annotationProcessorLibraries)
    coreLibraryDesugaring(Dependencies.coreLibraryDesugaringLibraries)
    testImplementation(Dependencies.testLibraries)
    androidTestImplementation(Dependencies.androidTestLibraries)
}