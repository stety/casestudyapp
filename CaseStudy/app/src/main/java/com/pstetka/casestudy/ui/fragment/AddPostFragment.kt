package com.pstetka.casestudy.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.pstetka.casestudy.R
import com.pstetka.casestudy.databinding.FragmentAddPostBinding
import com.pstetka.casestudy.model.CreatePostResult
import com.pstetka.casestudy.util.setOnSingleClickListener
import com.pstetka.casestudy.viewmodel.AddPostViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddPostFragment : Fragment(R.layout.fragment_add_post) {
    private val viewModel: AddPostViewModel by viewModel()
    private val binding by viewBinding(FragmentAddPostBinding::bind)
    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {
        binding.apply {
            fab.setOnSingleClickListener {
                createPost()
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.postCreatedResult.collectLatest { result ->
                        result?.let {
                            onPostCreatedResult(result)
                        }
                    }
                }
            }
        }
    }

    private fun createPost() {
        val content = binding.contentEditText.text?.toString()?.trim()
        var image = binding.imageEditText.text?.toString()?.trim()

        if (content.isNullOrBlank() || content.length < 6 || content.length > 50) {
            showSnackBar(getString(R.string.snackbar_content_of_post))
            return
        }

        if (image?.isBlank() == true)
            image = null

        viewModel.createPost(content, image)
    }

    private fun onPostCreatedResult(result: CreatePostResult) {
        if (result.isSuccess)
            navController.navigateUp()
        else
            showSnackBar(getString(R.string.snackbar_create_post_error))
    }

    private fun showSnackBar(text: String) {
        Snackbar.make(requireView(), text, Snackbar.LENGTH_LONG).show()
    }
}