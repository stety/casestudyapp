package com.pstetka.casestudy.persistence

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.pstetka.casestudy.model.Post

@Dao
interface PostDao {
    @Query("SELECT * FROM post WHERE id = :id")
    fun getById(id: String): LiveData<Post>

    @Query("SELECT * FROM post ORDER BY publishDate DESC")
    fun getAll(): PagingSource<Int, Post>

    @Insert(onConflict = REPLACE)
    suspend fun insert(post: Post)

    @Insert(onConflict = REPLACE)
    suspend fun insertAll(post: List<Post>)

    @Delete
    suspend fun delete(post: Post)

    @Query("DELETE FROM post")
    suspend fun deleteAll()
}