package com.pstetka.casestudy.di

import com.pstetka.casestudy.repository.PostRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { PostRepository(get(), get()) }
}