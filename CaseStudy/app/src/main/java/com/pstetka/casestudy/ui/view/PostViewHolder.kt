package com.pstetka.casestudy.ui.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pstetka.casestudy.R
import com.pstetka.casestudy.databinding.PostItemViewBinding
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.util.setOnSingleClickListener

class PostViewHolder(private val binding: PostItemViewBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(post: Post?, onMenuClicked: (view: View) -> Unit) {
        binding.apply {
            post?.let {
                Glide.with(itemView)
                    .load(post.image)
                    .placeholder(R.drawable.ic_baseline_image_64)
                    .into(image)

                username.text = "${post.owner.firstName} ${post.owner.lastName}"
                postText.text = post.text
            }

            menuButton.setOnSingleClickListener {
                onMenuClicked(binding.root)
            }
        }
    }
}