package com.pstetka.casestudy.api

import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.model.PostCreateBody
import com.pstetka.casestudy.model.PostDeleteResponse
import retrofit2.Response
import retrofit2.http.*

interface PostApi {
    @GET("post")
    suspend fun getPosts(
        @Query("limit") limit: Int,
        @Query("page") page: Int
    ): DataContainer<List<Post>>

    @POST("post/create")
    suspend fun createPost(@Body newPost: PostCreateBody): Response<Post?>

    @DELETE("post/{id}")
    suspend fun deletePost(@Path("id") id: String): Response<PostDeleteResponse>
}