package com.pstetka.casestudy.di

import com.pstetka.casestudy.viewmodel.AddPostViewModel
import com.pstetka.casestudy.viewmodel.PostsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { PostsViewModel(get()) }
    viewModel { AddPostViewModel(get()) }
}

