package com.pstetka.casestudy.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Entity
@Serializable
data class Post(
    @PrimaryKey
    val id: String,
    val image: String,
    val likes: Long,
    val text: String,
    val publishDate: Instant,
    @Embedded(prefix = "owner_")
    val owner: User
)