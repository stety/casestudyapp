package com.pstetka.casestudy.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Entity
@Serializable
data class User(
    @PrimaryKey
    val id: String,
    val firstName: String,
    val lastName: String,
    val title: String,
    val picture: String,
)