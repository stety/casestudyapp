package com.pstetka.casestudy

import android.app.Application
import com.pstetka.casestudy.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class App : Application() {
    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(
                listOf(
                    databaseModule,
                    viewModelModule,
                    apiModule,
                    repositoryModule,
                    retrofitModule
                )
            )

        }
        instance = this
    }
}