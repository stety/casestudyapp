package com.pstetka.casestudy.util

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import java.util.concurrent.atomic.AtomicBoolean

class OnSingleClickListener(
    private val clickListener: View.OnClickListener,
    private val intervalMs: Long = 1000
) : View.OnClickListener {
    private var canClick = AtomicBoolean(true)

    override fun onClick(v: View?) {
        if (canClick.getAndSet(false)) {
            v?.run {
                postDelayed({
                    canClick.set(true)
                }, intervalMs)
                clickListener.onClick(v)
            }
        }
    }
}


fun View.setOnSingleClickListener(function: () -> Unit) {
    val onClickListener = View.OnClickListener {
        function.invoke()
    }

    setOnClickListener(OnSingleClickListener(onClickListener))
}

fun <T : RecyclerView.ViewHolder> T.onClick(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnSingleClickListener {
        event.invoke(adapterPosition, itemViewType)
    }
    return this
}