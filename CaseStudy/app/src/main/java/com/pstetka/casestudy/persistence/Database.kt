package com.pstetka.casestudy.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.model.PostRemoteKey
import com.pstetka.casestudy.persistence.converter.InstantDateTimeConverter

@Database(entities = [Post::class, PostRemoteKey::class], version = 1)
@TypeConverters(InstantDateTimeConverter::class)
abstract class Database : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun postRemoteKeyDao(): PostRemoteKeyDao
}