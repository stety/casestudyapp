package com.pstetka.casestudy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.pstetka.casestudy.databinding.PostItemViewBinding
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.ui.view.PostViewHolder

class PostsAdapter(
    private val onMenuClicked: (holder: View, Post?) -> Unit
) : PagingDataAdapter<Post, PostViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding =
            PostItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return PostViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = getItem(position)
        holder.bind(post, onMenuClicked = { view ->
            onMenuClicked(view, post)
        })
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }
        }
    }
}