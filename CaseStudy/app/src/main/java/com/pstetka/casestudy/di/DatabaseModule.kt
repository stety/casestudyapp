package com.pstetka.casestudy.di

import android.app.Application
import androidx.room.Room
import com.pstetka.casestudy.persistence.Database
import com.pstetka.casestudy.persistence.PostDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single { database(androidApplication()) }
    single { postDao(get()) }
}

private fun database(application: Application): Database {
    val databaseName = "database"
    return Room.databaseBuilder(application, Database::class.java, databaseName)
        .fallbackToDestructiveMigration()
        .build()
}

private fun postDao(database: Database): PostDao {
    return database.postDao()
}