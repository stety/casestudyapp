package com.pstetka.casestudy.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import by.kirich1409.viewbindingdelegate.viewBinding
import com.pstetka.casestudy.R
import com.pstetka.casestudy.adapter.PostsAdapter
import com.pstetka.casestudy.databinding.FragmentPostsBinding
import com.pstetka.casestudy.databinding.PostItemViewBinding
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.ui.tools.GridSpacingInPxItemDecoration
import com.pstetka.casestudy.util.setOnSingleClickListener
import com.pstetka.casestudy.viewmodel.PostsViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostsFragment : Fragment(R.layout.fragment_posts) {
    private val viewModel: PostsViewModel by viewModel()
    private val binding by viewBinding(FragmentPostsBinding::bind)
    private val postsAdapter by lazy {
        PostsAdapter(onMenuClicked = { view, post ->
            showPopupMenu(view, post)
        })
    }
    private val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {
        binding.apply {
            recyclerView.apply {
                adapter = postsAdapter
                addItemDecoration(
                    GridSpacingInPxItemDecoration(
                        context,
                        POSTS_SPAN_COUNT,
                        resources.getDimensionPixelSize(R.dimen.post_spacing),
                        true
                    )
                )
            }

            swipeRefreshLayout.setOnRefreshListener {
                refreshPosts()
            }

            fab.setOnSingleClickListener {
                navController.navigate(PostsFragmentDirections.actionPostsFragmentToAddPostFragment())
            }

            viewLifecycleOwner.lifecycleScope.launch {
                postsAdapter.loadStateFlow.collectLatest { loadStates ->
                    if (loadStates.refresh is LoadState.NotLoading) {
                        if (swipeRefreshLayout.isRefreshing)
                            swipeRefreshLayout.isRefreshing = false

                        showInitialLoading(loadStates.append.endOfPaginationReached)
                    }
                }
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.posts.collectLatest { posts ->
                        onCollectPosts(posts)
                    }
                }
            }

            refreshPosts()
        }
    }

    private fun refreshPosts() {
        viewModel.fetchPosts()
    }

    private fun showInitialLoading(value: Boolean) {
        binding.progressCircular.isVisible = value
        binding.recyclerView.isVisible = !value
    }

    private fun showPopupMenu(view: View, post: Post?) {
        post?.let { _post ->
            context?.let { context ->
                val postItemBinding = PostItemViewBinding.bind(view)
                PopupMenu(context, postItemBinding.menuButton).apply {
                    setOnMenuItemClickListener {
                        when (it.itemId) {
                            R.id.actionDeletePost -> {
                                viewModel.deletePost(_post.id)
                            }
                        }
                        true
                    }
                    inflate(R.menu.menu_post)
                    show()
                }
            }
        }
    }

    private suspend fun onCollectPosts(posts: PagingData<Post>) {
        postsAdapter.submitData(posts)
    }

    companion object {
        const val POSTS_SPAN_COUNT = 2
    }
}