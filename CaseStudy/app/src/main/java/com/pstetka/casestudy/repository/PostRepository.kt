package com.pstetka.casestudy.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.pstetka.casestudy.api.PostApi
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.model.PostCreateBody
import com.pstetka.casestudy.persistence.Database
import com.pstetka.casestudy.util.networkCall
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext

/**
 * Repository implementation that uses a database backed [androidx.paging.PagingSource] and
 * [androidx.paging.RemoteMediator] to load posts from network and save them to Room database. When
 * loading from the API fails, the last stored value is loaded from the database.
 */
class PostRepository(
    private val postApi: PostApi,
    private val database: Database,
    private val dispatcher: CoroutineDispatcher = IO
) {
    private val postDao = database.postDao()

    @OptIn(ExperimentalPagingApi::class)
    fun getPosts(pageSize: Int): Flow<PagingData<Post>> = Pager(
        config = PagingConfig(pageSize),
        remoteMediator = PostsRemoteMediator(database, postApi)
    ) {
        postDao.getAll()
    }.flow.flowOn(dispatcher)

    suspend fun createPost(text: String, image: String?): Boolean = withContext(dispatcher) {
        val result = networkCall(apiCall = {
            postApi.createPost(
                PostCreateBody(
                    text,
                    image
                        ?: "https://img.dummyapi.io/photo-1564694202779-bc908c327862.jpg",
                    userId = "60d0fe4f5311236168a109ca" //TODO: For simplicity, we hardcode the userId and the default image
                )
            )
        })

        return@withContext result?.isSuccessful == true
    }

    suspend fun deletePost(id: String): Boolean = withContext(dispatcher) {
        val result = networkCall(apiCall = {
            postApi.deletePost(id)
        })

        return@withContext result?.isSuccessful == true
    }
}