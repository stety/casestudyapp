package com.pstetka.casestudy.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.pstetka.casestudy.BuildConfig
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

val retrofitModule = module {
    single { loggingInterceptor() }
    single { authorizationInterceptor() }
    single { kotlinSerialization() }
    single { okHttpClient(get(), get()) }
    single { retrofit(get(), get()) }
}

private fun loggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
    }
}

private fun authorizationInterceptor(): Interceptor {
    val appId = "6189c25b43d2673d1dd7e5c3"
    return Interceptor { chain ->
        chain.proceed(chain.request().newBuilder().apply {
            header("app-id", appId)
        }.build())
    }
}


@ExperimentalSerializationApi
private fun kotlinSerialization(): Converter.Factory {
    val contentType = "application/json".toMediaType()
    val json = Json { ignoreUnknownKeys = true }
    return json.asConverterFactory(contentType)
}

private fun retrofit(okHttpClient: OkHttpClient, kotlinSerialization: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .addConverterFactory(kotlinSerialization)
        .client(okHttpClient)
        .build()
}

private fun okHttpClient(
    loggingInterceptor: HttpLoggingInterceptor,
    authorizationInterceptor: Interceptor
): OkHttpClient {
    val connectTimeout = 15L
    val writeTimeout = 15L
    val readTimeout = 15L

    return OkHttpClient.Builder().apply {
        connectTimeout(connectTimeout, TimeUnit.SECONDS)
        writeTimeout(writeTimeout, TimeUnit.SECONDS)
        readTimeout(readTimeout, TimeUnit.SECONDS)
        addInterceptor(loggingInterceptor)
        addInterceptor(authorizationInterceptor)
    }.build()
}