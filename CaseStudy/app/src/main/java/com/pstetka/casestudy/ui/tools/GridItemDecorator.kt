package com.pstetka.casestudy.ui.tools

import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import kotlin.math.roundToInt

class GridSpacingInPxItemDecoration(
    private val context: Context,
    private val spanCount: Int,
    private val spacingInPxInDp: Int,
    private val includeEdge: Boolean
) : ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val spacingInPx = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            spacingInPxInDp.toFloat(),
            context.resources.displayMetrics
        ).roundToInt()

        val position = parent.getChildAdapterPosition(view)
        val column = position % spanCount
        if (includeEdge) {
            outRect.left = spacingInPx - column * spacingInPx / spanCount
            outRect.right = (column + 1) * spacingInPx / spanCount

            if (position < spanCount) {
                outRect.top = spacingInPx
            }
            outRect.bottom = spacingInPx
        } else {
            outRect.left = column * spacingInPx / spanCount
            outRect.right = spacingInPx - (column + 1) * spacingInPx / spanCount

            if (position >= spanCount) {
                outRect.top = spacingInPx
            }
        }
    }
}