package com.pstetka.casestudy.api

import kotlinx.serialization.Serializable

@Serializable
data class DataContainer<T>(
    val data: T? = null,
    val total: Long,
    val page: Long,
    val limit: Long
)