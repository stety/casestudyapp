package com.pstetka.casestudy.persistence.converter

import androidx.room.TypeConverter
import kotlinx.datetime.Instant

class InstantDateTimeConverter {
    @TypeConverter
    fun fromTimestamp(value: String?): Instant? {
        return value?.let { Instant.parse(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Instant?): String? {
        return date?.toString()
    }
}
