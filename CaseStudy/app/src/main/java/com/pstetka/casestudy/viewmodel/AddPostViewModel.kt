package com.pstetka.casestudy.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pstetka.casestudy.model.CreatePostResult
import com.pstetka.casestudy.repository.PostRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class AddPostViewModel(
    private val postRepository: PostRepository
) : ViewModel() {
    private val _postCreatedResult: MutableStateFlow<CreatePostResult?> = MutableStateFlow(null)
    val postCreatedResult: StateFlow<CreatePostResult?> = _postCreatedResult

    fun createPost(text: String, image: String?) = viewModelScope.launch {
        val response = postRepository.createPost(text, image)

        _postCreatedResult.value = when (response) {
            true -> CreatePostResult(true)
            else -> CreatePostResult(false)
        }
    }
}