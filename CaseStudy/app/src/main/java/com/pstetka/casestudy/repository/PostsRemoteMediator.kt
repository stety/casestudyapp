package com.pstetka.casestudy.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.pstetka.casestudy.api.PostApi
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.model.PostRemoteKey
import com.pstetka.casestudy.persistence.Database
import com.pstetka.casestudy.persistence.PostRemoteKeyDao
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

@OptIn(ExperimentalPagingApi::class)
class PostsRemoteMediator(
    private val database: Database,
    private val postApi: PostApi,
    private val initialPage: Int = 0
) : RemoteMediator<Int, Post>() {
    private val postDao = database.postDao()
    private val postRemoteKeyDao: PostRemoteKeyDao = database.postRemoteKeyDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Post>
    ): MediatorResult {
        return try {
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: initialPage
                }
                LoadType.PREPEND -> {
                    return MediatorResult.Success(true)
                }
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                        ?: throw InvalidObjectException("Result is empty.")
                    remoteKeys.nextKey ?: return MediatorResult.Success(true)
                }
            }

            val posts = postApi.getPosts(
                page = page,
                limit = state.config.pageSize,
            ).data

            if (posts != null) {
                val endOfPaginationReached = posts.size < state.config.pageSize

                database.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        postDao.deleteAll()
                        postRemoteKeyDao.deleteAll()
                    }

                    val prevKey = if (page == initialPage) null else page - 1
                    val nextKey = if (endOfPaginationReached) null else page + 1
                    val keys = posts.map {
                        PostRemoteKey(postId = it.id, prevKey = prevKey, nextKey = nextKey)
                    }

                    postRemoteKeyDao.insertAll(keys)
                    postDao.insertAll(posts)
                }

                MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
            } else
                throw InvalidObjectException("Response is empty.")
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        } catch (e: Exception) {
            MediatorResult.Error(e)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Post>): PostRemoteKey? {
        return state.lastItemOrNull()?.let { post ->
            database.withTransaction { postRemoteKeyDao.getByPostId(post.id) }
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, Post>): PostRemoteKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                database.withTransaction { postRemoteKeyDao.getByPostId(id) }
            }
        }
    }
}
