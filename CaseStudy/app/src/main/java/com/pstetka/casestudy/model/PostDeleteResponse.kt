package com.pstetka.casestudy.model

import kotlinx.serialization.Serializable

@Serializable
data class PostDeleteResponse(
    val id: String
)