package com.pstetka.casestudy.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.pstetka.casestudy.model.Post
import com.pstetka.casestudy.repository.PostRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PostsViewModel(
    private val postRepository: PostRepository
) : ViewModel() {
    private val _posts: MutableStateFlow<PagingData<Post>> = MutableStateFlow(PagingData.empty())
    val posts: StateFlow<PagingData<Post>> = _posts

    fun fetchPosts() = viewModelScope.launch {
        postRepository.getPosts(POST_PAGE_SIZE)
            .cachedIn(viewModelScope)
            .collectLatest { posts ->
                _posts.value = posts
            }
    }

    fun deletePost(id: String) = viewModelScope.launch {
        if (postRepository.deletePost(id))
            fetchPosts()
    }

    companion object {
        private const val POST_PAGE_SIZE = 20
    }
}
