package com.pstetka.casestudy.model

data class CreatePostResult(val isSuccess: Boolean)