package com.pstetka.casestudy.di

import com.pstetka.casestudy.api.PostApi
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single(createdAtStart = false) { get<Retrofit>().create(PostApi::class.java) }
}