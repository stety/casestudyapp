package com.pstetka.casestudy.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.pstetka.casestudy.model.PostRemoteKey

@Dao
interface PostRemoteKeyDao {
    @Insert(onConflict = REPLACE)
    fun insertAll(postRemoteKey: List<PostRemoteKey>)

    @Query("SELECT * FROM PostRemoteKey WHERE postId = :postId")
    fun getByPostId(postId: String): PostRemoteKey

    @Query("DELETE FROM PostRemoteKey")
    suspend fun deleteAll()
}