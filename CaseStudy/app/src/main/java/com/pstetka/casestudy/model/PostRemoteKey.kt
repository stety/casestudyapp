package com.pstetka.casestudy.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Entity
@Serializable
data class PostRemoteKey(
    @PrimaryKey
    val postId: String,
    val prevKey: Int?,
    val nextKey: Int?
)