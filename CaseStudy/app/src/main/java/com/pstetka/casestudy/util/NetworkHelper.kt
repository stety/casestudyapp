package com.pstetka.casestudy.util

import com.pstetka.casestudy.api.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import retrofit2.Response


/**
 * Function that can provide a resource backed by both the sqlite database and the network.
 *
 * @param <ResultType>
 * @param <RequestType>
 */

inline fun <ResultType, RequestType> networkBoundResource(
    crossinline query: () -> Flow<ResultType>,
    crossinline fetch: suspend () -> Response<DataContainer<RequestType>>,
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
    crossinline onFetchFailed: (Throwable) -> Unit = { },
    crossinline shouldFetch: (ResultType) -> Boolean = { true },
    crossinline processResponse: (Response<DataContainer<RequestType>>) -> RequestType = { response -> response.body()!!.data!! }
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data)) {
        emit(Resource.Loading(data))

        try {
            val response = fetch()

            if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    query().map { Resource.Success(it) }
                } else {
                    if (body.data != null)
                        saveFetchResult(processResponse(response))
                    else
                        throw ApiErrorResponseException("Field 'data' in response is null!")

                    query().map { Resource.Success(it) }
                }
            } else {
                var message = response.errorBody()?.string()

                if (message.isNullOrEmpty()) {
                    message = response.message()
                }

                throw ApiErrorResponseException(message ?: "unknown network error")
            }
        } catch (throwable: Throwable) {
            onFetchFailed(throwable)
            query().map { Resource.Error(throwable, it) }
        }
    } else {
        query().map { Resource.Success(it) }
    }

    emitAll(flow)
}.flowOn(IO)

/**
 * Function for processing network states when calling network api calls
 */
suspend inline fun <T> networkCall(
    crossinline apiCall: suspend () -> Response<T>,
    crossinline onEmptyResponse: (Response<T>) -> Unit = { },
    crossinline onSuccess: (Response<T>) -> Unit = { },
    crossinline onFailed: (Throwable) -> Unit = { },
): Response<T>? = withContext(IO) {
    try {
        val response = apiCall()

        if (response.isSuccessful) {
            val body = response.body()

            if (body == null || response.code() == 204) {
                onEmptyResponse(response)
            }
            onSuccess(response)

            return@withContext response
        } else {
            var message = response.errorBody()?.string()

            if (message.isNullOrEmpty()) {
                message = response.message()
            }

            throw ApiErrorResponseException(message ?: "unknown network error")
        }
    } catch (throwable: Throwable) {
        onFailed(throwable)
        return@withContext null
    }
}

class ApiErrorResponseException(message: String) : Exception(message)