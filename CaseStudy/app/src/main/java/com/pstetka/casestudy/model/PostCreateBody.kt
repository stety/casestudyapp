package com.pstetka.casestudy.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PostCreateBody(
    val text: String,
    val image: String? = null,
    val likes: Long? = null,
    val tags: List<String>? = null,
    @SerialName("owner")
    val userId: String
)