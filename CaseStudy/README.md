# CaseStudy

Example Android app containing following technologies as demo:

- MVVM architecture
- Kotlin Flows
- Kotlin Coroutines
- Room
- Kotlin DateTime
- Retrofit and OkHttp
- Kotlin Serialization Converter
- Koin
- REST api
- Navigation
- View Binding
- Glide
- KTS

App show you posts from dummy api. You can create new posts and delete existing posts.